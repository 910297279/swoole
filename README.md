# swoole
1. 基于swoole实现的消息推送系统，开箱即用
2. 可以二次开发为OA系统、直播间、项目消息推送系统

# 演示站点
1. 未封装ws:http://swoole.phpyd.com（已停用，如需使用看下面注意）
2. 已封装wss:https://www.phpyd.com/addons/swoole

# 启动服务器
```
php server.php
```

# 访问客户端
1. 配置虚拟域名到websocket，浏览器访问index.html

![显示文本](https://mystudy1.oss-cn-shenzhen.aliyuncs.com/mayun/swoole_index.png)

![显示文本](https://mystudy1.oss-cn-shenzhen.aliyuncs.com/mayun/swoole_room.png)

# 目前功能
1. 全站公告
2. 房间内广播
3. 组别内广播
4. 房间内互相播送
5. 用户禁言
6. 用户送礼
7. 踢出用户
8. 当前在线人数

# op缓存管理地址
http://域名/ocp.php，如我的是：
http://swoole.phpyd.com/ocp.php

# 使用技术说明
使用了如下技术
1. Redis（存储用户权限等数据，已使用）
2. Swoole的Websocket（消息推送，已使用）
3. Swoole的内存表（存储用户数据，已使用）
4. Swoole的数据库连接池（有实例，目前暂未使用）
5. Swoole的异步任务（有实例，目前暂未使用）

#注意：
目前使用https支持的wss协议，ws协议已停用，如需切换为ws，server.php文件：

$server = new swoole_websocket_server('0.0.0.0', 9501, SWOOLE_BASE, SWOOLE_SOCK_TCP | SWOOLE_SSL);
改为：
$server = new swoole_websocket_server('0.0.0.0', 9501);
