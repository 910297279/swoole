#!/bin/bash

count=`ps -fe |grep "server.php" | grep -v "grep" | wc -l`

if [ $count -lt 1 ]; then
ps -eaf |grep "server.php" | grep -v "grep"| awk '{print $2}'|xargs kill -9
sleep 2
ulimit -c unlimited
/usr/local/php/bin/php /data/wwwroot/swoole/websocket/server.php
echo $(date +%Y-%m-%d_%H:%M:%S) >/data/wwwroot/swoole/websocket/restart.log
fi
