<?php

/************************************************************
 * Copyright (C), 1993, Dacelve. Tech., Ltd.
 * FileName : TcpServer.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/1/17 14:52
 * Description   :
 * Function List :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/1/17   1.0          init
 ***********************************************************/
class TcpServer
{
    public $serv;
    protected $conf = [
        'send_interval_time' => 3000,//群发的间隔时间（单位：毫秒）
        'send_continuance_time' => 3000,//群发的持续时间（单位：秒）
    ];

    public function __construct($host = '127.0.0.1', $port = '9501')
    {
        $this->serv = new swoole_server($host, $port);
    }

    public function receive($callBack){
        //监听数据接收事件
        $this->serv->on('receive', $callBack);
    }
}